package net.ouska.utils;

public class Strings {

	public static boolean isEmpty(String value) {
		return value == null ? false : value.trim().isEmpty();
	}
	
	public static int wordCount(String value) {
		return words(value).length;
	}

	public static String[] words(String value) {
		if (isEmpty(value)) {
			return new String[0];
		}
		value = value.replaceAll("[\\W]", " ");
		value = value.replaceAll("\\s+", " ");
		return value.split("\\s");
	}
	
	public static String bytesToHex(byte[] hashInBytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
    		int i = b & 0xFF;
            sb.append(Integer.toHexString(i));
        }
        return sb.toString();
    }	

	public static char shiftCharacter(char znak, int posun) {
		if (znak == ' ') {
			return znak;
		}
		// 97 - 122
		posun = posun % 26;
		znak += posun;
		if (znak > 122) {
			znak -= 26;
		}
		if (znak < 97) {
			znak += 26;
		}
		return znak;
    }	
}
