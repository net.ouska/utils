package net.ouska.utils;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class IO {

	private static Scanner scanner = new Scanner(System.in);
	private static Random random = new Random(System.currentTimeMillis());
	
	public static void println() {
		System.out.println();
	}

	public static void print(String text) {
		System.out.print(text);
	}


	public static void println(Object object) {
		if (object != null) {
			System.out.println(object.toString());
		}
	}

	public static void print(Object object) {
		if (object != null) {
			System.out.print(object.toString());
		}
	}

	public static void println(String text) {
		System.out.println(text);
	}

	public static String toString(int[] array) {
		StringBuilder b = new StringBuilder("[");
		if (array != null) {
			for (int i : array) {
				if (b.length() > 1) {
					b.append(",");	
				}
				b.append(i);
			}
		}
		b.append("]");
		return b.toString(); 
	}

	public static void print(int[] array) {
		print(toString(array));
	}

	public static void print(char[] array) {
		StringBuilder b = new StringBuilder("[");
		if (array != null) {
			for (char i : array) {
				if (b.length() > 1) {
					b.append(",");	
				}
				b.append(i);
			}
		}
		b.append("]");
		print(b.toString()); 
	}

	public static void println(int[] array) {
		print(array);
		println();
	}

	public static void println(char[] array) {
		print(array);
		println();
	}

	public static void print(int number) {
		System.out.print(number);
	}

	public static void println(char ch) {
		System.out.println(ch);
	}

	public static void print(char ch) {
		System.out.print(ch);
	}

	public static void println(int number) {
		System.out.println(number);
	}

	public static void print(long number) {
		System.out.print(number);
	}

	public static void println(long number) {
		System.out.println(number);
	}

	public static void print(float number) {
		System.out.print(number);
	}

	public static void println(float number) {
		System.out.println(number);
	}

	public static void print(double number) {
		System.out.print(number);
	}

	public static void println(double number) {
		System.out.println(number);
	}

	public static void print(boolean value) {
		System.out.print(value);
	}

	public static void println(boolean value) {
		System.out.println(value);
	}

	public static String readLine() {
		try {
			return scanner.nextLine();	
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
	}

	public static String read() {
		try {
			return scanner.next();
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
	}
	
	public static int readInt() {
		try {
			return scanner.nextInt();
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
	}

	public static long readLong() {
		try {
			return scanner.nextLong();
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
	}

	public static float readFloat() {
		return scanner.nextFloat();
	}

	public static double readDouble() {
		return scanner.nextDouble();
	}
	
	public static String readFile(File file, String charset) {
		try (Scanner sc = new Scanner(file, charset)) {
			StringBuilder b = new StringBuilder();
			while (sc.hasNextLine()) {
				if (b.length() > 0) {
					b.append(System.lineSeparator());	
				}
				b.append(sc.nextLine());
			}
			return b.toString();
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
	}

	public static List<String> readFileLines(File file, String charset) {
		try (Scanner sc = new Scanner(file, charset)) {
			List<String> lines = new ArrayList<String>();
			while (sc.hasNextLine()) {
				lines.add(sc.nextLine());
			}
			return lines;
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
	}

	public static List<String> readFileLinesUTF8(String filePath) {
		return readFileLines(new File(filePath), "utf-8");
	}

	public static List<String> readFileLinesCP1250(String filePath) {
		return readFileLines(new File(filePath), Charset.defaultCharset().name());
	}

	public static String readFileUTF8(String filePath) {
		return readFile(new File(filePath), "utf-8");
	}

	public static String readFileCP1250(String filePath) {
		return readFile(new File(filePath), Charset.defaultCharset().name());
	}

	public static void writeFile(File file, String charset, String content) {
		try (OutputStreamWriter fstream = new OutputStreamWriter(new FileOutputStream(file), charset)) {
			fstream.write(content);
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
	}
	
	public static void close(Closeable c) {
		try {
			if ( c!= null) {
				c.close();	
			}
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
	}
	
	public static void closeQuitly(Closeable c) {
		close(c);
	}
	
	public static String readURL(String url, String charset) {
		URL u = null;
		try {
			u = new URL(url);
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
		
		try (BufferedReader in = new BufferedReader(new InputStreamReader(u.openStream(), charset))) {
			StringBuilder b = new StringBuilder();

        	String inputLine;
	        while ((inputLine = in.readLine()) != null) {
				if (b.length() > 0) {
					b.append(System.lineSeparator());	
				}
				b.append(inputLine);
	        }
	        return b.toString();
		} catch (Exception e) {
			throw new RuntimeException("Chyba pri cteni" + e.getMessage());
		}
	}
	
	/**
	 * <min ; max)
	 * @param min incl
	 * @param max excl
	 * @return
	 */
	public static int randomInt(int min, int max) {
		return random.nextInt(max - min) + min;
	}

	public static int[] randomInts(int size, int min, int max) {
		int[] result = new int[size];
		for (int i = 0; i < result.length; i++) {
			result[i] = randomInt(min, max);
		}
		return result;
	}

	public static char randomCharacter() {
		return (char) (randomInt(0, 26) + 'a');
	}

	public static char[] randomDNA(int size) {
		char[] dna = new char[] {'a','c','g','t'};
		char[] result = new char[size];
		for (int i = 0; i < result.length; i++) {
			result[i] = dna[randomInt(0, 4)];
		}
		return result;
	}

	public static String randomString() {
		String text = 
		"Nov� str�nky se obvykle ve wiki vytv��ej� jednodu�e p�id�n�m odpov�daj�c�ho odkazu na tematicky p��buznou str�nku Pokud odkazovan� str�nka neexistuje je odkaz obvykle zv�razn�n jako nefunk�n� P�echod na takov� odkaz otev�e edita�n� okno kter� umo�n� u�ivateli napsat text nov� str�nky Tento mechanismus zaji��uje �e str�nky str�nky na n� nen� odjinud odkazov�no vznikaj� jen zcela v�jime�n� a zachov�v� tak vysokou �rove� hypertextov� prov�zanosti str�nek" + 
		"Wiki syst�my jsou obecn� zalo�eny na principu �e je lep�� usnad�ovat opravy chyb ne� br�nit jejich vzniku Proto wiki i kdy� jsou hodn� otev�en� z�rove� poskytuj� r�zn� prost�edky pro kontrolu platnosti posledn�ch zm�n obsahu Nejv�razn�j�� v t�m�� ka�d�m wiki syst�mu je str�nka posledn�ch zm�n recent changes page kter� zobrazuje seznam ur�it�ho po�tu posledn�ch zm�n nebo v�echny zm�ny za ur�it� �asov� obdob� N�kter� wiki umo��uj� seznam filtrovat tak aby mal� zm�ny nebo zm�ny prov�d�n� skripty boty byly vynech�ny"; 
		String[] split = text.split(" ");
		return split[randomInt(0, split.length)];
	}

	public static String[] randomStrings(int size) {
		String[] result = new String[size];
		for (int i = 0; i < result.length; i++) {
			result[i] = randomString();
		}
		return result;
	}
	
	public static long time() {
		return System.currentTimeMillis();
	}

	public static String md5(String text) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
		    md.update(text.getBytes());
		    byte[] digest = md.digest();
		    return Strings.bytesToHex(digest).toUpperCase();		
		} catch (NoSuchAlgorithmException e) {
			System.err.println("No algorithm " + e.getMessage());
			throw new RuntimeException("No algorithm " + e.getMessage());
		}
	}
	
	public static void pause(long millis) {
		try {
			Thread.sleep(millis);	
		} catch (Exception e) {
			
		}
	}
}

